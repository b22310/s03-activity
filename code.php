<?php

$nameObj= (object)[

	'firstName'=> 'Senku',
	'middleName' => '',
	'lastName' => 'Ishigami'

];

// Objects from Classes

class Person{

	// properties
	public $firstName;
	public $middleName;
	public $lastName;

	public function __construct($firstName, $middleName, $lastName){
		$this-> firstName= $firstName;
		$this -> middleName= $middleName;
		$this -> lastName= $lastName;
	}
	// Method

	public function printName(){
		return "Your full name is $this->firstName $this->lastName";
	}
}


$person = new Person('Senku', '', 'Ishigami');

// Inheritance & Polymorphism

class Developer extends Person{
	public function printName(){
		return "Your name is $this->firstName $this->middleName $this->lastName and you are a developer";
	}

}

$developer= new Developer('John', 'Finch', 'Smith');


class Engineer extends Person{
	public function printName(){
		return "You are an engineer named $this->firstName $this->middleName $this->lastName.";
	}

}

$engineer= new Engineer('Harold', 'Myers', 'Reese');

